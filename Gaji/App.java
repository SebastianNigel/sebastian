package com.nexsoft.Gaji;

import java.util.ArrayList;

public class App extends GajiKar{
    public static void main(String[] args) {   
    
    Data obj1 = new Data ("Sebastian", 1 , "A1" , 3000000);
    Data obj2 = new Data ("Nigel\t", 2 , "A2" , 3000000);
    Data obj3 = new Data ("Sebastian", 3 , "A3" , 3000000);
    Data obj4 = new Data ("Sebastian", 4 , "B1" , 10000000);
    Data obj5 = new Data ("Sebastian", 5 , "B2" , 10000000);
    
    ArrayList<Data> list = new ArrayList<>();
    list.add(obj1);
    list.add(obj2);
    list.add(obj3);
    list.add(obj4);
    list.add(obj5);
    
    hasil(list);
    }

    public static void hasil(ArrayList<Data>list){
        System.out.println("Nama  \t ID Karyawan  \t Parking ID \t Gaji  \t");
        for (int i = 0 ; i <list.size(); i++){
            System.out.print(list.get(i).getNama()+"\t");
            System.out.print(list.get(i).getID()+"\t");
            System.out.print(list.get(i).getPark()+"\t");
            System.out.print(list.get(i).getGaji()+"\n");
        }
    }
}

package com.nexsoft.Gaji;

public class Data extends GajiKar{
    Data(String newNama , Integer newID , String newPark , long newGaji ){
        super.setNama(newNama);
        super.setID(newID);
        super.setPark(newPark);
        super.setGaji(newGaji);
    }

        public void showData(){
            System.out.println("Nama = "+getNama());
            System.out.println("ID Karyawan = "+getID());
            System.out.println("Nomor Parkir = "+getPark());
            System.out.println("Gaji = "+getGaji());
        }
}